package cs535.storm.twittercount;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LossyCountingBolt extends BaseRichBolt {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7490251620431329888L;

    private OutputCollector collector;

    private Map<String, HashtagPacket> buckets;
    private int bucketID;
    private long totalHashtags;
    private int windowSize;
    private float thresholdFrequency;
    private float epsilon;
    
    private long lastPushTime;
    private long pushInterval;

    public LossyCountingBolt(Float thresholdFrequency, Long pushInterval, Integer windowSize) {
        this.windowSize = windowSize;
        this.thresholdFrequency = thresholdFrequency;
        this.pushInterval = pushInterval*1000;
        this.epsilon = 1.0f/windowSize;
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector collector) {
    	this.collector = collector;
        this.buckets = new ConcurrentHashMap<String, HashtagPacket>();
        this.bucketID = 1;
        this.totalHashtags = 0l;
        this.lastPushTime = System.currentTimeMillis();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("hashtag", "frequency", "delta"));
    }

    @Override
    public void execute(Tuple input) {
        String word = ((String) input.getValueByField("hashtag")).toLowerCase();
        
        if (!buckets.containsKey(word)) {
            buckets.put(word, new HashtagPacket(word, 1L, bucketID-1));
        } else {
        	buckets.get(word).setFrequency(buckets.get(word).getFrequency() + 1);
        }
        
       totalHashtags = Long.parseLong((String) input.getValueByField("total"));
       
        if (totalHashtags % windowSize == 0) {
        	for(Iterator<Map.Entry<String, HashtagPacket>> it = buckets.entrySet().iterator(); it.hasNext(); ) {
        	      Map.Entry<String, HashtagPacket> entry = it.next();
        	      if(entry.getValue().getFrequency() + entry.getValue().getDelta() <= bucketID) {
        	        it.remove();
        	      }
        	    }
        	bucketID++;
        }
        
        long now = System.currentTimeMillis();
        if (now - lastPushTime >= pushInterval) {
            for (Map.Entry<String, HashtagPacket> entry : buckets.entrySet()) {
                if (entry.getValue().getFrequency() >= (thresholdFrequency-epsilon)*totalHashtags) {
                	collector.emit(new Values(entry.getValue().getHashtagText(), entry.getValue().getFrequency(), entry.getValue().getDelta()));
                }
            }
            lastPushTime = now;
        }
    }
}
