package cs535.storm.twittercount;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;

public class Topology {

	static final String TOPOLOGY = "CS535TwitterHashtagCount";
	static final String PARALLELISM = "Parallel";

	public static void main(String[] args) {
		String fileName = args[0];
		long pushInterval = Integer.parseInt(args[1]);
		Float thresholdFrequency = Float.parseFloat(args[2]);
		Integer windowSize = Integer.parseInt(args[3]);
		Integer maxHashtagCount = Integer.parseInt(args[4]);
		Integer isParallel = Integer.parseInt(args[5]);
		
		if (thresholdFrequency == 0) {
			thresholdFrequency = 1F/windowSize;
		}

		Config config = new Config();

		TopologyBuilder b = new TopologyBuilder();
		String TOPOLOGY_NAME;

		if (isParallel == 1) {
			TOPOLOGY_NAME = TOPOLOGY + PARALLELISM;
			b.setSpout("TwitterSpout", new TwitterSpout());
			b.setBolt("FindHashtagBolt", new FindHashtagBolt()).globalGrouping("TwitterSpout");
			b.setBolt("LoggingBolt", new LoggingBolt(fileName, pushInterval)).globalGrouping("TwitterSpout");
			b.setBolt("LossyCountingBolt", new LossyCountingBolt(thresholdFrequency, pushInterval/2, windowSize), 4).fieldsGrouping("FindHashtagBolt", new Fields("hashtag"));
			b.setBolt("OutputBolt", new OutputBolt(pushInterval, maxHashtagCount)).globalGrouping("LossyCountingBolt");
			config.setNumWorkers(4);
		} else {
			TOPOLOGY_NAME = TOPOLOGY;
			b.setSpout("TwitterSpout", new TwitterSpout());
			b.setBolt("FindHashtagBolt", new FindHashtagBolt()).globalGrouping("TwitterSpout");
			b.setBolt("LoggingBolt", new LoggingBolt(fileName, pushInterval)).globalGrouping("TwitterSpout");
			b.setBolt("LossyCountingBolt", new LossyCountingBolt(thresholdFrequency, pushInterval/2, windowSize)).globalGrouping("FindHashtagBolt");
			b.setBolt("OutputBolt", new OutputBolt(pushInterval, maxHashtagCount)).globalGrouping("LossyCountingBolt");

			//final LocalCluster cluster = new LocalCluster();
			//cluster.submitTopology(TOPOLOGY_NAME, config, b.createTopology());

			/*try {
				Thread.sleep(100000);
			} catch (Exception e)  {

			}*/

			//cluster.killTopology(TOPOLOGY_NAME);
			//cluster.shutdown();
		}
		
		try {
			StormSubmitter.submitTopologyWithProgressBar(TOPOLOGY_NAME, config, b.createTopology());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
