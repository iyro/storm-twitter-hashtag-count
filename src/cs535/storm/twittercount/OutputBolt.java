package cs535.storm.twittercount;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by iyro on 11/13/15.
 */
public class OutputBolt extends BaseRichBolt {
    private int maxHashtagCount;
    private ConcurrentHashMap<String, HashtagPacket> buckets;
    private long lastPushTime;

    DateFormat formatter;

    private long pushInterval;

    public OutputBolt(long pushInterval, int maxHashtagCount) {
        this.pushInterval = pushInterval*1000;
        this.maxHashtagCount = maxHashtagCount;
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        this.buckets = new ConcurrentHashMap<String, HashtagPacket>();
        this.lastPushTime = System.currentTimeMillis();
        this.formatter = new SimpleDateFormat("[HH:mm:ss]");
    }

    @Override
    public void execute(Tuple tuple) {
        String word = ((String) tuple.getValueByField("hashtag"));
        Long frequency = ((Long) tuple.getValueByField("frequency"));
        Integer delta = ((Integer) tuple.getValueByField("delta"));

        buckets.put(word, new HashtagPacket(word, frequency, delta));
        
        while(true) {
        	String minHashtag = "";
	        if (buckets.size() > maxHashtagCount) {
	            for (String hashtag : buckets.keySet()) {
	                if (minHashtag.isEmpty() || buckets.get(minHashtag).getFrequency() + buckets.get(minHashtag).getDelta() > buckets.get(hashtag).getDelta() + buckets.get(hashtag).getFrequency()) {
	                    minHashtag = hashtag;
	                }
	            }
	
	           buckets.remove(minHashtag);
	        } else {
	        	break;
	        }
        }

        long now = System.currentTimeMillis();
        if (now - lastPushTime >= pushInterval) {
            ArrayList<Map.Entry<String, HashtagPacket>> list = new ArrayList<Map.Entry<String, HashtagPacket>>(buckets.entrySet());
            Collections.sort( list, new Comparator<Map.Entry<String, HashtagPacket>>()
                    {
                        public int compare( Map.Entry<String, HashtagPacket> o1, Map.Entry<String, HashtagPacket> o2 )
                        {
                            return ((Long)(o2.getValue().getFrequency())).compareTo((Long)( o1.getValue().getFrequency()));
                        }
                    } );

            int outputCount = 0;
            StringBuilder sb = new StringBuilder();
            sb.append(formatter.format(new Date(now))).append("####Trending####");
            sb.append(System.getProperty("line.separator"));
            for (Map.Entry<String, HashtagPacket> entry : list) {
                sb.append((outputCount+1) + ". #" + entry.getValue().getHashtagText() + ", " + entry.getValue().getFrequency() + ", " + entry.getValue().getDelta());
                sb.append(System.getProperty("line.separator"));
                outputCount++;
            }
            System.out.println(sb.toString());
            lastPushTime = now;
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
