package cs535.storm.twittercount;

public class HashtagPacket {
	private String hashtagText;
	private Long frequency;
	private Integer delta;
	
	public HashtagPacket(String hashtagText, Long frequency, Integer delta) {
		super();
		this.hashtagText = hashtagText;
		this.frequency = frequency;
		this.delta = delta;
	}

	public String getHashtagText() {
		return hashtagText;
	}

	public void setHashtagText(String hashtagText) {
		this.hashtagText = hashtagText;
	}

	public Long getFrequency() {
		return frequency;
	}

	public void setFrequency(Long frequency) {
		this.frequency = frequency;
	}

	public Integer getDelta() {
		return delta;
	}

	public void setDelta(Integer delta) {
		this.delta = delta;
	}
}
