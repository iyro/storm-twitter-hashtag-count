package cs535.storm.twittercount;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import twitter4j.HashtagEntity;
import twitter4j.Status;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class LoggingBolt extends BaseRichBolt {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3490385493186567409L;
	long start;
	long logPushInterval;
	
	String fileName;
	DateFormat formatter;
	
	PrintWriter log;
	
	ConcurrentHashMap<Long, ArrayList<String>> hashTagList;
	
	public LoggingBolt(String fileName, long pushInterval) {
		super();
		this.fileName = fileName;
		this.logPushInterval = pushInterval;
	}

	@Override
	public void execute(Tuple tuple) {
		//System.out.println("Execute logger called!");
		long now = System.currentTimeMillis();
		
		Status tweet = (Status) tuple.getValueByField("tweet");
        HashtagEntity[] hashtags = tweet.getHashtagEntities();
        
        for (HashtagEntity hashtag : hashtags) {
        	if (!hashTagList.containsKey(now)) {
    			hashTagList.put(now, new ArrayList<String>());
    		}
    		hashTagList.get(now).add(hashtag.getText().toLowerCase());
        }		
		
        if (now - start >= logPushInterval) {
        	for (Long timestamp : hashTagList.keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append(formatter.format(new Date(timestamp)));                
        		for (String tag : hashTagList.get(timestamp)) {
        			sb.append("<" + tag + "> ");
        		}
        		sb.append(System.getProperty("line.separator"));
        		        		
        		log.write(sb.toString());       
        		log.flush();
        	}
        	
        	hashTagList = new ConcurrentHashMap<Long, ArrayList<String>>();
        }
	}

	@Override
	public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
		start = System.currentTimeMillis();
		this.hashTagList = new ConcurrentHashMap<Long, ArrayList<String>>();
		this.formatter = new SimpleDateFormat("[HH:mm:ss:SSS]");
		
		File file = new File(fileName);
		
		if (file.exists()) {
			file.delete();
		}
		
		try {
			log = new PrintWriter(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		
	}
	
	@Override
	public void cleanup() {
		log.close();
	}

}
