package cs535.storm.twittercount;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import twitter4j.HashtagEntity;
import twitter4j.Status;

import java.util.Map;

public class FindHashtagBolt extends BaseRichBolt {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7612394729573617162L;
	private OutputCollector collector;
	private Long total = 0L;

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(Tuple input) {
        Status tweet = (Status) input.getValueByField("tweet");
        HashtagEntity[] hashtags = tweet.getHashtagEntities();
        for (HashtagEntity hashtag : hashtags) {
        	total++;
            collector.emit(new Values(hashtag.getText(), total.toString()));
            //System.out.println(total.toString());
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("hashtag", "total"));
    }
}
